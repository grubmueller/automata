# automata

Emulate various automata, such as DFA, NFA, PDA and DTM.

This software is implemented in Rust and uses [`pest`](https://pest.rs/) for automaton configuration parsing, [`clap`](https://github.com/clap-rs/clap) for command line argument interpretation and [`anyhow`](https://github.com/dtolnay/anyhow) for error handling.
