pub struct DFAArgs {
    pub config: std::string::String,
    pub input: std::string::String,
}

pub struct NFAArgs {
    pub config: std::string::String,
    pub input: std::string::String,
}
pub struct PDAArgs {
    pub config: std::string::String,
    pub input: std::string::String,
}

pub struct DTMArgs {
    pub config: std::string::String,
    pub input: std::string::String,
}

pub enum AutomatonArgs {
    DFA(DFAArgs),
    NFA(NFAArgs),
    PDA(PDAArgs),
    DTM(DTMArgs),
    None,
}

pub struct Arguments {
    pub show_license: bool,
    pub automaton_args: AutomatonArgs,
}

pub struct OutputConf {
    pub is_tty: bool,
    pub term_width: usize,
}

pub const DEFAULT_TERM_WIDTH: usize = 100;
