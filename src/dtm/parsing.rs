//    automata
//    Copyright (C) 2021  Fabian Lukas Grubmüller
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

//! utilities for turing machine configuration parsing

use super::tools::{DTMConf, DTMDirection, DTMTape, DTM};
use anyhow::bail;
use pest::Parser;
use pest_derive::Parser;
use std::convert::TryFrom;

/// DDTM parsing struct created by `pest`
#[derive(Parser)]
#[grammar = "dtm/dtm.pest"]
pub struct DTMParser;

/// deterministic turing machine configuration parsing
///
/// takes the turing machine configuration and tape configuration input and returns a turing machine if successful
pub fn parse(config: &str, input: &str, term_width: usize) -> anyhow::Result<DTM> {
    let mut alphabet: std::collections::HashSet<&str> = std::collections::HashSet::new();
    let mut states: std::collections::HashMap<&str, Option<bool>> =
        std::collections::HashMap::new(); // Some(true) -> accept; Some(false) -> deny; None -> not terminal
    let mut init = "";
    let mut rules: std::collections::HashSet<(
        Option<&str>,
        Option<&str>,
        &str,
        &str,
        DTMDirection,
    )> = std::collections::HashSet::new();

    // parse turing machine configuration

    let tmconf = DTMParser::parse(Rule::conf, config)?.next().unwrap();

    for line in tmconf.into_inner() {
        match &line.as_rule() {
            Rule::init => {
                let s = line.into_inner().next().unwrap().as_str();
                if !states.contains_key(s) {
                    states.insert(s, None);
                }
                init = s;
            }
            Rule::accept => {
                for ident in line.into_inner().next().unwrap().into_inner() {
                    let s = ident.as_str();
                    states.insert(s, Some(true));
                }
            }
            Rule::deny => {
                for ident in line.into_inner().next().unwrap().into_inner() {
                    let s = ident.as_str();
                    states.insert(s, Some(false));
                }
            }
            Rule::action => {
                let mut item = line.into_inner();
                let current_state = match item.next().unwrap().as_str() {
                    "*" => None,
                    s => {
                        if !states.contains_key(s) {
                            states.insert(s, None);
                        }
                        Some(s)
                    }
                };
                let current_symbol = match item.next().unwrap().as_str() {
                    "*" => None,
                    s => {
                        alphabet.insert(s);
                        Some(s)
                    }
                };
                let next_state = item.next().unwrap().as_str();
                if !states.contains_key(next_state) {
                    states.insert(next_state, None);
                }
                let change_symbol = item.next().unwrap().as_str();
                alphabet.insert(change_symbol);
                let direction = match item.next().unwrap().as_str() {
                    "l" | "L" | "<" => DTMDirection::Left,
                    "r" | "R" | ">" => DTMDirection::Right,
                    "n" | "N" | "." => DTMDirection::Neutral,
                    _ => bail!("Unable to parse direction"),
                };
                rules.insert((
                    current_state,
                    current_symbol,
                    next_state,
                    change_symbol,
                    direction,
                ));
            }
            _ => (),
        }
    }

    // parse tape configuration

    let mut tape: std::vec::Vec<Option<&str>> = std::vec::Vec::new();

    let tapeconf = DTMParser::parse(Rule::tape, input)?.next().unwrap();

    for cell in tapeconf.into_inner() {
        match &cell.as_rule() {
            Rule::ident => match cell.as_str() {
                "_" => tape.push(None),
                str => {
                    tape.push(Some(str));
                    alphabet.insert(str);
                }
            },
            _ => (),
        }
    }

    // initialise turing machine configuration

    let (alphabet, symb_max_len) = {
        let mut temp: std::vec::Vec<std::string::String> =
            std::vec::Vec::with_capacity(alphabet.len());
        let mut tempsml: usize = 0;
        for item in alphabet {
            let symb = item.to_string();
            let symb_len = symb.len();
            temp.push(item.to_string());
            tempsml = std::cmp::max(tempsml, symb_len);
        }
        (temp, tempsml)
    };

    let alphabet_transl = {
        let size = alphabet.len();
        let mut temp: std::collections::HashMap<&str, usize> =
            std::collections::HashMap::with_capacity(size);
        for i in 0..size {
            temp.insert(&alphabet[i], i);
        }
        temp
    };

    let (states, terms) = {
        let size = states.len();
        let mut temps: std::vec::Vec<std::string::String> = std::vec::Vec::with_capacity(size);
        let mut tempt: std::vec::Vec<Option<bool>> = std::vec::Vec::with_capacity(size);
        for (key, val) in states {
            temps.push(key.to_string());
            tempt.push(val);
        }
        (temps, tempt)
    };

    let states_transl = {
        let size = states.len();
        let mut temp: std::collections::HashMap<&str, usize> =
            std::collections::HashMap::with_capacity(size);
        for i in 0..size {
            temp.insert(&states[i], i);
        }
        temp
    };

    let rules = {
        let mut temp: std::collections::HashMap<
            (usize, Option<usize>),
            (usize, Option<usize>, DTMDirection),
        > = std::collections::HashMap::with_capacity(rules.len());
        for (cst, csy, nst, nsy, dir) in rules.iter() {
            // (Option<&str>, Option<&str>, &str, &str, DTMDirection)
            let (states_iter, next_state) = match cst {
                Some(st) => {
                    let cst_n = *states_transl.get(st).unwrap();
                    let nst_n = if nst == st {
                        cst_n
                    } else {
                        *states_transl.get(nst).unwrap()
                    };
                    (vec![cst_n], nst_n)
                }
                None => {
                    // wildcard
                    let nst_n = *states_transl.get(nst).unwrap();
                    let range: std::vec::Vec<usize> = (0..states.len() - 1).collect();
                    (range, nst_n)
                }
            };
            let (symbols_iter, next_symbol) = match csy {
                Some(sy) => {
                    let csy_n = match sy {
                        &"_" => None,
                        sy => Some(*alphabet_transl.get(sy).unwrap()),
                    };
                    let nsy_n = {
                        if nsy == sy {
                            csy_n
                        } else {
                            match nsy {
                                &"_" => None,
                                nsy => Some(*alphabet_transl.get(nsy).unwrap()),
                            }
                        }
                    };
                    (vec![csy_n], nsy_n)
                }
                None => {
                    // wildcard
                    let nsy_n = match nsy {
                        &"_" => None,
                        sy => Some(*alphabet_transl.get(sy).unwrap()),
                    };
                    let mut range: std::vec::Vec<Option<usize>> = (0..states.len() - 1)
                        .map(|x| -> Option<usize> { Some(x) })
                        .collect();
                    range.push(None);
                    (range, nsy_n)
                }
            };
            for current_state in states_iter.iter() {
                for current_symbol in symbols_iter.iter() {
                    let ret = temp.insert(
                        (*current_state, *current_symbol),
                        (next_state, next_symbol, *dir),
                    );
                    if ret != None {
                        bail!("Error: ambiguous definition of transition function");
                        // double assignment
                    }
                }
            }
        }
        temp
    };

    // initialise tape configuration

    let data = {
        let mut temp: std::collections::VecDeque<Option<usize>> =
            std::collections::VecDeque::with_capacity(tape.len());
        for symb in tape.iter() {
            match symb {
                None => temp.push_back(None),
                Some(s) => temp.push_back(Some(*alphabet_transl.get(s).unwrap())),
            }
        }
        temp
    };

    let state = *states_transl.get(init).unwrap();

    let conf = DTMConf {
        alphabet,
        states,
        terms,
        rules,
    };

    let number_cells_to_display = (term_width - 9) / (symb_max_len + 3);

    let (window_begin, window_end, look_ahead) =
        match isize::try_from(number_cells_to_display).unwrap() {
            0 => {
                bail!("Unable to display tape due to excessive symbol length");
            } // unlikely
            1 => (0, 1, 0),  // unlikely
            2 => (0, 2, 0),  // unlikely
            3 => (-1, 2, 1), // unlikely
            4 => (-1, 3, 1), // unlikely
            5 => (-2, 3, 2), // unlikely
            6 => (-2, 4, 2), // unlikely
            b => (0, b, 0),  // (-3, b - 3, 3), // likely
        };

    let tape = DTMTape {
        data,
        pos: 0,
        window_begin,
        window_end,
        look_ahead,
    };

    let machine = DTM {
        conf,
        tape,
        state,
        symb_max_len,
    };

    Ok(machine)
}
