//    automata
//    Copyright (C) 2021  Fabian Lukas Grubmüller
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

//! provides structs and basic functions for the turing machine

use std::convert::TryFrom;

/// next direction
#[derive(Debug, Hash, Clone, Copy, PartialEq, Eq)]
pub enum DTMDirection {
    Left,
    Right,
    Neutral,
}

/// turing machine general configuration
#[derive(Debug)]
pub struct DTMConf {
    pub alphabet: std::vec::Vec<std::string::String>,
    pub states: std::vec::Vec<std::string::String>,
    pub terms: std::vec::Vec<Option<bool>>, // Some(true) -> accept; Some(false) -> deny; None -> not terminal
    pub rules:
        std::collections::HashMap<(usize, Option<usize>), (usize, Option<usize>, DTMDirection)>,
}

/// turing machine tape
#[derive(Debug)]
pub struct DTMTape {
    pub data: std::collections::VecDeque<Option<usize>>,
    pub pos: isize,
    pub window_begin: isize,
    pub window_end: isize,
    pub look_ahead: usize,
}

impl DTMTape {
    pub fn collect_window(&self) -> (std::vec::Vec<Option<usize>>, bool, bool) {
        let mut window: std::vec::Vec<Option<usize>> = std::vec::Vec::with_capacity(
            usize::try_from(self.window_end - self.window_begin).unwrap(),
        );

        for i in self.window_begin..self.window_end {
            window.push(self.get(i));
        }

        let continues_left = self.window_begin > 0;
        let continues_right = self.window_end < isize::try_from(self.data.len()).unwrap();
        (window, continues_left, continues_right)
    }

    pub fn get(&self, index: isize) -> Option<usize> {
        if index < 0 || index >= isize::try_from(self.data.len()).unwrap() {
            None
        } else {
            self.data[usize::try_from(index).unwrap()]
        }
    }

    /// get the position relative to the window
    pub fn get_rel_pos(&self) -> isize {
        self.pos - self.window_begin
    }

    pub fn move_left(&mut self) {
        let len = isize::try_from(self.data.len()).unwrap();
        if self.pos > 0 {
            self.pos -= 1;
            if self.pos - self.window_begin < isize::try_from(self.look_ahead).unwrap() {
                self.window_begin -= 1;
                self.window_end -= 1;
            }
            if self.pos == len - 2 && *self.data.back().unwrap() == None {
                self.data.pop_back();
            }
        } else {
            if len == 1 {
                // if tape data structure only contains one cell and this cell is blank, then avoid `pop_back();push_front(None)`
                if self.data[0] != None {
                    self.data.push_front(None);
                }
            } else {
                self.data.push_front(None);
            }
        }
    }

    pub fn move_right(&mut self) {
        let mut try_move_window = false;
        let len = isize::try_from(self.data.len()).unwrap();

        if self.pos < len - 1 {
            if self.pos == 0 && self.data[0] == None {
                self.data.pop_front();
                self.window_begin -= 1;
                self.window_end -= 1;
            } else {
                self.pos += 1;
            }
            try_move_window = true;
        } else {
            if len == 1 {
                // if tape data structure only contains one cell and this cell is blank, then avoid `pop_front();push_back(None)`
                if self.data[0] != None {
                    self.data.push_back(None);
                    self.pos += 1;
                    try_move_window = true;
                }
            } else {
                self.data.push_back(None);
                self.pos += 1;
                try_move_window = true;
            }
        }
        if try_move_window {
            if self.window_end - self.pos < isize::try_from(self.look_ahead).unwrap() + 1 {
                self.window_begin += 1;
                self.window_end += 1;
            }
        }
    }
}

#[allow(dead_code)]
//#[test]
fn test_move_right() -> anyhow::Result<()> {
    let mut data: std::collections::VecDeque<Option<usize>> =
        std::collections::VecDeque::with_capacity(10);

    for i in 0..10 {
        data.push_back(Some(i));
    }

    let mut tape1 = DTMTape {
        data,
        pos: 0,
        window_begin: 0,
        window_end: 5,
        look_ahead: 0,
    };

    for i in 1..10 {
        tape1.move_right();
        assert_eq!(tape1.pos, i);
        assert_eq!(tape1.data.len(), 10);
        if i < 5 {
            assert_eq!(tape1.window_begin, 0);
            assert_eq!(tape1.window_end, 5);
        }
    }

    tape1.move_right();
    assert_eq!(tape1.pos, 10);
    assert_eq!(tape1.data.len(), 11);
    assert_eq!(tape1.window_begin, 6);
    assert_eq!(tape1.window_end, 11);

    Ok(())
}

#[test]
fn test_move_right_put_none() -> anyhow::Result<()> {
    let mut data: std::collections::VecDeque<Option<usize>> =
        std::collections::VecDeque::with_capacity(10);

    for i in 0..10 {
        data.push_back(Some(i));
    }

    let mut tape1 = DTMTape {
        data,
        pos: 0,
        window_begin: 0,
        window_end: 5,
        look_ahead: 0,
    };

    for i in 1..10 {
        tape1.data[usize::try_from(tape1.pos).unwrap()] = None;
        tape1.move_right();
        assert_eq!(tape1.pos, 0);
        assert_eq!(tape1.data.len(), usize::try_from(10 - i).unwrap());
        assert_eq!(tape1.window_begin, 0);
        assert_eq!(tape1.window_end, 5);
    }

    tape1.data[usize::try_from(tape1.pos).unwrap()] = None;
    tape1.move_right();
    println!("{}", tape1.pos);
    assert_eq!(tape1.pos, 0);
    assert_eq!(tape1.data.len(), 1);
    assert_eq!(tape1.window_begin, 0);
    assert_eq!(tape1.window_end, 5);

    Ok(())
}

/// turing machine
#[derive(Debug)]
pub struct DTM {
    pub conf: DTMConf,
    pub tape: DTMTape,
    pub state: usize,
    pub symb_max_len: usize, // in TM since this should affect every tape
}
