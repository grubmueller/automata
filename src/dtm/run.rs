//    automata
//    Copyright (C) 2021  Fabian Lukas Grubmüller
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

extern crate anyhow;

use super::tools::{DTMDirection, DTMTape, DTM};
use anyhow::bail;
use std::convert::TryFrom;
use std::{thread, time};
use DTMDirection::{Left, Neutral, Right};

fn action(t: &mut DTMTape, nsy: Option<usize>, dir: DTMDirection) {
    // overwrite current symbol
    t.data[usize::try_from(t.pos).unwrap()] = nsy;

    // move
    match dir {
        Left => t.move_left(),
        Right => t.move_right(),
        Neutral => (),
    }
}

pub fn run(mut machine: DTM) -> anyhow::Result<()> {
    // println!("——< Initial configuration >———\n");
    super::printing::print(&machine);

    loop {
        let csy = machine.tape.get(machine.tape.pos);
        let (nst, nsy, dir) = match machine.conf.rules.get(&(machine.state, csy)) {
            None => match csy {
                None => bail!(
                    "{}",
                    error_msg_incomplete_transition_function(
                        &machine.conf.states[machine.state],
                        " "
                    )
                ),
                Some(sy) => bail!(
                    "{}",
                    error_msg_incomplete_transition_function(
                        &machine.conf.states[machine.state],
                        &machine.conf.alphabet[sy]
                    )
                ),
            },
            Some(im) => *im,
        };

        action(&mut machine.tape, nsy, dir);
        machine.state = nst;

        thread::sleep(time::Duration::from_millis(0));
        // println!(
        //     "{} {} -> {} {} {}",
        //     machine.conf.states[machine.state],
        //     match csy {
        //         Some(sy) => &machine.conf.alphabet[sy],
        //         None => "_",
        //     },
        //     machine.conf.states[nst],
        //     match nsy {
        //         Some(sy) => &machine.conf.alphabet[sy],
        //         None => "_",
        //     },
        //     match dir {
        //         Left => "L",
        //         Right => "R",
        //         Neutral => "N",
        //     }
        // );
        super::printing::print(&machine);

        match machine.conf.terms[machine.state] {
            Some(_) => break,
            None => (),
        };
    }
    Ok(())
}

fn error_msg_incomplete_transition_function(cst: &str, csy: &str) -> std::string::String {
    let mut msg =
        "No next action but current state is non-terminal\n\nMachine is currently in state `"
            .to_string();
    msg.push_str(cst);
    msg.push_str("` with head at symbol `");
    msg.push_str(csy);
    msg.push_str("` and there is no way to proceed since the transition function is incomplete.\n\nHint: add the following line to the configuration:\n\n");
    msg.push_str(cst);
    msg.push(' ');
    msg.push_str(csy);
    msg.push_str(" -> <nst> <nsy> <dir>\n\nwhere `<nst>` is the next state, `<nsy>` is the next symbol and `<dir>` is the direction of the next movement.");
    msg
}
