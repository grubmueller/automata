//    automata
//    Copyright (C) 2021  Fabian Lukas Grubmüller
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

use super::tools::{DTMTape, DTM};
use std::convert::TryFrom;

const CONTINUES_LEFT_TRUE: &str = &"<<=";
const CONTINUES_RIGHT_TRUE: &str = &"=>>";
const CONTINUES_FALSE: &str = &":::";
const CONTINUES_LEFT_FALSE: &str = CONTINUES_FALSE;
const CONTINUE_RIGHT_FALSE: &str = CONTINUES_FALSE;

/// print the current turing machine configuration, including state and tapes
pub fn print(machine: &DTM) {
    // print state
    println!(
        "machine state: {}{}",
        &machine.conf.states[machine.state],
        match machine.conf.terms[machine.state] {
            Some(true) => &" [accept]",
            Some(false) => &" [deny]",
            None => &"",
        }
    );

    println!();

    // print tapes
    print_tapes(&machine.tape, &machine.conf.alphabet, machine.symb_max_len);

    println!();
}

/// print the wholes of all tapes
///
/// TODO: implement multiple tapes
fn print_tapes(t: &DTMTape, alphabet: &std::vec::Vec<std::string::String>, symb_max_len: usize) {
    print_tape(t, alphabet, symb_max_len);
}

/// print one whole tape
fn print_tape(t: &DTMTape, alphabet: &std::vec::Vec<std::string::String>, symb_max_len: usize) {
    let (window, continues_left, continues_right) = t.collect_window();
    let rel_pos = t.get_rel_pos();

    let len = 9 + window.len() * (symb_max_len + 3);
    let current_begin = 4 + usize::try_from(rel_pos).unwrap() * (symb_max_len + 3);
    let current_end = current_begin + symb_max_len + 4;

    print_head_line(current_begin, current_end, '/', '|', '\\');

    print_sep(len, '—');

    print_cond(continues_left, CONTINUES_LEFT_TRUE, CONTINUES_LEFT_FALSE);

    for elem in window.iter() {
        print!(" | ");
        match *elem {
            Some(u) => print_cell_content(&alphabet[u], symb_max_len),

            None => print_cell_content(&"", symb_max_len),
        }
    }
    print!(" | ");

    print_cond(continues_right, CONTINUES_RIGHT_TRUE, CONTINUE_RIGHT_FALSE);

    println!();

    print_sep(len, '—');

    print_head_line(current_begin, current_end, '\\', '|', '/');
}

/// prints the aligned content of a cell
///
/// `str.len() <= width` is required
pub fn print_cell_content(str: &str, width: usize) {
    let rest = width - str.len();
    let r = rest / 2;
    let l = rest - r;
    for _i in 0..l {
        print!("{}", ' ');
    }
    print!("{}", str);
    for _i in 0..r {
        print!("{}", ' ');
    }
}

/// print a line containing a visual depiction of the scanner head
///
/// prints `<l><m>*<r>` from column `begin` to (excluding) column `end`
fn print_head_line(begin: usize, end: usize, l: char, m: char, r: char) {
    for _i in 0..begin {
        print!("{}", ' ');
    }
    print!("{}", l);
    for _i in begin + 1..end - 1 {
        print!("{}", m);
    }
    println!("{}", r);
}

fn print_sep(n: usize, c: char) {
    for _i in 0..n {
        print!("{}", c);
    }
    println!();
}

fn print_cond(cond: bool, iftrue: &str, iffalse: &str) {
    if cond {
        print!("{}", iftrue);
    } else {
        print!("{}", iffalse);
    }
}
