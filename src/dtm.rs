//    automata
//    Copyright (C) 2021  Fabian Lukas Grubmüller
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

mod parsing;
mod printing;
mod run;
mod tools;

pub fn dtm(
    args: crate::argument_tools::DTMArgs,
    output_conf: crate::argument_tools::OutputConf,
) -> anyhow::Result<()> {
    let machine = parsing::parse(&args.config, &args.input, output_conf.term_width)?;

    run::run(machine)
}
