//    automata
//    Copyright (C) 2021  Fabian Lukas Grubmüller
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

//! emulate a turing maching

mod argument_tools;
mod arguments;
mod dfa;
mod dtm;
mod licenses;
mod nfa;
mod pda;

use argument_tools::AutomatonArgs::{None, DFA, DTM, NFA, PDA};

/// main function
fn main() -> anyhow::Result<()> {
    let (arguments, output_conf)  = arguments::arguments()?;

    if arguments.show_license {
        licenses::print();
        return Ok(());
    }

    match arguments.automaton_args {
        DFA(args) => dfa::dfa(args, output_conf),
        NFA(args) => nfa::nfa(args, output_conf),
        PDA(args) => pda::pda(args, output_conf),
        DTM(args) => dtm::dtm(args, output_conf),
        None => Ok(()),
    }
}
