//    automata
//    Copyright (C) 2021  Fabian Lukas Grubmüller
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

use crate::argument_tools::{
    Arguments, AutomatonArgs, DFAArgs, DTMArgs, NFAArgs, OutputConf, PDAArgs,
};
use anyhow::bail;
use clap::{
    crate_authors, crate_description, crate_name, crate_version, App, Arg, ArgMatches, SubCommand,
};
use crossterm::{terminal::size, tty::IsTty};
use std::io::stdout;

pub fn arguments() -> anyhow::Result<(Arguments, OutputConf)> {
    let matches = App::new(crate_name!())
        .version(crate_version!())
        .author(crate_authors!())
        .about(crate_description!())
        .arg(
            Arg::with_name("license")
                .help("Display the license of this program and all of the used libraries.")
                .short("l")
                .long("license")
                .takes_value(false),
        )
        .subcommand(
            SubCommand::with_name("dfa")
                .version(crate_version!())
                .author(crate_authors!())
                .about("Emulate a deterministic finite automaton")
                .display_order(0)
                .arg(
                    Arg::with_name("config")
                        .help("The path of the automaton configuration")
                        .number_of_values(1)
                        .value_name("CONFIG")
                        .index(1)
                        .required_unless("license"),
                )
                .arg(
                    Arg::with_name("word")
                        .help("The input word")
                        .number_of_values(1)
                        .value_name("INPUT")
                        .index(2)
                        .required_unless("license"),
                ),
        )
        .subcommand(
            SubCommand::with_name("nfa")
                .version(crate_version!())
                .author(crate_authors!())
                .about("Emulate a non-deterministic finite automaton")
                .display_order(1)
                .arg(
                    Arg::with_name("config")
                        .help("The path of the automaton configuration")
                        .number_of_values(1)
                        .value_name("CONFIG")
                        .index(1)
                        .required_unless("license"),
                )
                .arg(
                    Arg::with_name("word")
                        .help("The input word")
                        .number_of_values(1)
                        .value_name("INPUT")
                        .index(2)
                        .required_unless("license"),
                ),
        )
        .subcommand(
            SubCommand::with_name("pda")
                .version(crate_version!())
                .author(crate_authors!())
                .about("Emulate a push down automaton")
                .display_order(2)
                .arg(
                    Arg::with_name("config")
                        .help("The path of the automaton configuration")
                        .number_of_values(1)
                        .value_name("CONFIG")
                        .index(1)
                        .required_unless("license"),
                )
                .arg(
                    Arg::with_name("word")
                        .help("The input word")
                        .number_of_values(1)
                        .value_name("INPUT")
                        .index(2)
                        .required_unless("license"),
                ),
        )
        .subcommand(
            SubCommand::with_name("dtm")
                .version(crate_version!())
                .author(crate_authors!())
                .about("Emulate a deterministic turing machine")
                .display_order(3)
                .arg(
                    Arg::with_name("config")
                        .help("The path of the automaton configuration")
                        .number_of_values(1)
                        .value_name("CONFIG")
                        .index(1)
                        .required_unless("license"),
                )
                .arg(
                    Arg::with_name("word")
                        .help("The input word")
                        .number_of_values(1)
                        .value_name("INPUT")
                        .index(2)
                        .required_unless("license"),
                ),
        )
        .get_matches();

    let show_license = matches.is_present("license");

    let output_conf = if stdout().is_tty() {
        OutputConf {
            is_tty: true,
            term_width: size()?.0.into(),
        }
    } else {
        OutputConf {
            is_tty: false,
            term_width: crate::argument_tools::DEFAULT_TERM_WIDTH,
        }
    };

    let arguments = match matches.subcommand_name() {
        Some("dfa") => {
            let sub_matches = matches.subcommand_matches("dfa").unwrap();
            Arguments {
                show_license,
                automaton_args: AutomatonArgs::DFA(DFAArgs {
                    config: get_config(sub_matches)?,
                    input: get_input(sub_matches)?,
                }),
            }
        }
        Some("nfa") => {
            let sub_matches = matches.subcommand_matches("nfa").unwrap();
            Arguments {
                show_license,
                automaton_args: AutomatonArgs::NFA(NFAArgs {
                    config: get_config(sub_matches)?,
                    input: get_input(sub_matches)?,
                }),
            }
        }
        Some("pda") => {
            let sub_matches = matches.subcommand_matches("pda").unwrap();
            Arguments {
                show_license,
                automaton_args: AutomatonArgs::PDA(PDAArgs {
                    config: get_config(sub_matches)?,
                    input: get_input(sub_matches)?,
                }),
            }
        }
        Some("dtm") => {
            let sub_matches = matches.subcommand_matches("dtm").unwrap();
            Arguments {
                show_license,
                automaton_args: AutomatonArgs::DTM(DTMArgs {
                    config: get_config(sub_matches)?,
                    input: get_input(sub_matches)?,
                }),
            }
        }
        _ => Arguments {
            show_license,
            automaton_args: AutomatonArgs::None,
        },
    };
    Ok((arguments, output_conf))
}

fn get_config(matches: &ArgMatches) -> anyhow::Result<std::string::String> {
    match matches.value_of_os("config") {
        Some(path) => Ok(std::fs::read_to_string(path)?),
        None => Ok("".to_owned()),
    }
}

fn get_input(matches: &ArgMatches) -> anyhow::Result<std::string::String> {
    match matches.value_of_os("word") {
        Some(osstr) => match osstr.to_owned().into_string() {
            Ok(str) => Ok(str),
            Err(_) => bail!("Unable to read input word: not a valid character sequence".to_owned()),
        },
        None => Ok("".to_owned()),
    }
}
